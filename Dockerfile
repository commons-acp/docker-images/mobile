## base image
#FROM ubuntu:20.04
#
## block interactive actions (ex selecting country during installation)
#ARG DEBIAN_FRONTEND=noninteractive
#
## install linux packages
#RUN apt-get update && apt-get -y install wget tar unzip openjdk-8-jdk build-essential checkinstall file apt-utils libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
## Install Android SDK in the Ubuntu 20.04 LTS
#RUN sudo mkdir ~/DevTools && sudo mkdir ~/DevTools/Android && sudo mkdir ~/DevTools/Android/cmdline-tools && unzip commandlinetools-linux-6200805_latest.zip && mv tools ~/DevTools/Android/cmdline-tools/
#RUN sudo nano ~/.bashrc && echo "DEV_TOOLS= \"/home/$USER/DevTools\" JAVA_HOME=\"$DEV_TOOLS/JDK/jdk-11.0.7+10\" JAVA_HOME=\"$DEV_TOOLS/JDK/jdk-11.0.7+10\"  JAVA_HOME=\"$DEV_TOOLS/JDK/jdk-11.0.7+10\" ANDROID_HOME=\"$DEV_TOOLS/Android\""
#
#
#
#RUN source ~/.bashrc
#RUN sdkmanager "platform-tools" "platforms;android-29"
#RUN sdkmanager "build-tools" "build-tools;29.0.3"
#RUN sdkmanager --licenses
FROM beevelop/android-nodejs

ENV CORDOVA_VERSION 10.0.0

WORKDIR "/tmp"

RUN npm i -g --unsafe-perm cordova@${CORDOVA_VERSION} && \
    cordova -v && \
    cd /tmp && \
    cordova create CHOSA com.myCompany.CHOSA CHOSA && \
    cd CHOSA && \
    cordova plugin add cordova-plugin-camera --save && \
    cordova platform add android --save && \
    cordova requirements android && \
    cordova build android --verbose && \
    rm -rf /tmp/CHOSA \
#CMD ["cordova", "run android"]
VOLUME ["[ChosaAndroid]"]
RUN echo "done"
